package com.vedclasses.in.ved;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VedApplication {

	public static void main(String[] args) {
		SpringApplication.run(VedApplication.class, args);
	}

}
